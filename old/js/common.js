
//ロールオーバー
$(function(){	
	$(".over a").hover(
	function() {
		var src = $(this).children("img").attr('src').replace("_off.", "_on.");
		$(this).children("img").attr('src',src);
		$(this).children("img").fadeTo(0, 0.3).fadeTo(300, 1);
		$(this).children("img").fadeIn(500);
		$(this).children("img").queue([]);		
	},
	function() {
		var src = $(this).children("img").attr('src').replace("_on.", "_off.");
		$(this).children("img").attr('src',src);	
	});	
	
});
